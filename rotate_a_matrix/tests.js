var compareArrays = function (array1,array2) {
	if (array1.length !== array2.length) {
		return false;
	}
	if (Array.isArray(array1[0]) === true) {
		if (Array.isArray(array2[0]) === false) {
			return false;
		}
		if (compareArrays(array1[0],array2[0]) === false) {
			return false;
		}
	} else {
		if (array1[0] !== array2[0]) {
			return false;
		}
	}
	if (array1.length < 2) {
		return true;
	} else {
		return compareArrays(array1.slice(1),array2.slice(1));
	}
};

assertArraysAreTheSame = function (array1,array2) {
	if (compareArrays(array1,array2) !== true) {
		console.error("array: ",JSON.stringify(array1)," is not similar to ",JSON.stringify(array2));
		throw new Error();
	}
};

assertArraysAreTheSame(
	rotateRight([[1,2],[4,3]]),
	[[4,1],[3,2]]
);

/*
assertArraysAreTheSame(
	rotateRight([[1]]),
	[[1]]
);

assertArraysAreTheSame(
	transpose([[1,2],[4,3]]),
	[[1],[]]
);

assertArraysAreTheSame(
	rotateRight([[1,2],[4,3]]),
	[[4,1],[3,2]]
);
*/
