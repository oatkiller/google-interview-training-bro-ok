rotateRight = function (array) {
	var i = 0,
		j = 0,
		temporary;

	for (i = 0; i < array.length; i++) {
		for (j = 0; j < i; j++) {
			temporary = array[i][j];
			array[i][j] = array[j][i];
			array[j][i] = temporary;
		}
	}

	for (j = 0; j < array.length / 2; j++) {
		for (i = 0; i < array.length; i++) {
			temporary = array[i][j];
			array[i][j] = array[i][array.length - 1 - j];
			array[i][array.length - 1 - j] = temporary;
		}
	}

/*
	for (i = 0; i < array.length / 2; i++) {
		for (j = 0; j < array.length; j++) {
			temporary = array[i][j];
			array[i][j] = array[array.length - 1 - i][j];
			array[array.length - 1 - i][j] = temporary;
		}
	}
	*/

	return array;

};
