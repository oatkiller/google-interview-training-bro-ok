Array.prototype.permutations = function () {
	return this.reduce(
		function (memo,element,index,array) {
			return memo.concat(
				[
					[element]
				].
					concat(
						array.
							slice(index + 1).
							permutations().
							map(function (permutation) {
								return [element].concat(permutation);
							})
					)
			);
		},
		[]
	);
};

/*
console.log("a".split("").permutations());
console.log("ab".split("").permutations());
console.log("abc".split("").permutations());
console.log("abcd".split("").permutations());
console.log("abcde".split("").permutations());
*/

var stuff = "abcdef".split("").permutations();
for (var i = 0; i < stuff.length; i++) {

	console.log(stuff[i]);
}
