var onAssertion;
var handleOnAssertion = function (assertionText) {
	console.log("passed: " + assertionText);
	assertEquals.passedCount++;
	if (typeof onAssertion === "function") {
		onAssertion();
	}
};
var assertEquals = function (actual,expected,assertionText) {
	if (actual !== expected) {
		console.log("FAILED ASSERTION: " + assertionText);
	} else {
		handleOnAssertion(assertionText);
	}
};
assertEquals.printPassedCount = function () {
	console.log("Passed " + this.passedCount + " assertions!");
};
assertEquals.passedCount = 0;
var assertRejected = function (promise,assertionText) {
	promise.then(function () {
		console.log("FAILED ASSERTION: " + assertionText);
	},function () {
		handleOnAssertion(assertionText);
	});
};
var assertResolved = function (promise,assertionText) {
	promise.then(function () {
		handleOnAssertion(assertionText);
	},function () {
		console.log("FAILED ASSERTION: " + assertionText);
	});
};
var fail = function (assertionText) {
	console.log("FAILED ASSERTION: " + assertionText);
};
var addTests = function (test) {
	if (test === undefined) {
		assertEquals.printPassedCount();
	} else {
		var otherTests = Array.prototype.slice.call(arguments,1);
		onAssertion = function () {
			addTests.apply(undefined,otherTests);
		};
		test();
	}
};

addTests(
	function () {
		fail("no tests");	
	}
);
