var BinarySearchTree = {
	Node : function (key,value,parentNode) {
		this.key = key;
		this.value = value;
		this.parentNode = parentNode;
	}
};

BinarySearchTree.Node.prototype.compareTo = function (secondKey) {
	var firstKey = this.key;
	if (typeof firstKey === "number") {
		return firstKey < secondKey ? -1 : firstKey > secondKey ? 1 : 0;
	} else if (typeof firstKey === "string") {
		return firstKey.localeCompare(secondKey);
	} else {
		throw new Error("no valid comparator provided");
	}
};

BinarySearchTree.Node.prototype.insert = function (key,value) {
	if (this.key === key) {
		this.value = value;
	} else if (this.compareTo(key) > 0) {
		if (this.leftChild === undefined) {
			this.leftChild = new BinarySearchTree.Node(key,value,this);
		} else {
			this.leftChild.insert(key,value);
		}
	} else {
		if (this.rightChild === undefined) {
			this.rightChild = new BinarySearchTree.Node(key,value,this);
		} else {
			this.rightChild.insert(key,value);
		}
	}
};

BinarySearchTree.Node.prototype.find = function (key) {
	if (this.key === key) {
		return this;
	} else {
		if (this.compareTo(key) > 0) {
			return this.leftChild.find(key);
		} else {
			return this.rightChild.find(key);
		}
	}
};

BinarySearchTree.Node.prototype.replace = function (node) {
	if (this.parentNode !== undefined) {
		if (this.parentNode.leftChild === this) {
			this.parentNode.leftChild = node;
		} else if (this.parentNode.rightChild === this) {
			this.parentNode.rightChild = node;
		}
	}
	if (node !== undefined) {
		node.parentNode = this.parentNode;
	}
};

BinarySearchTree.Node.prototype.findMinimumNode = function () {
	var currentNode = this;
	while (currentNode.leftChild !== undefined) {
		currentNode = currentNode.leftChild;
	}
	return currentNode;
};

BinarySearchTree.Node.prototype.traverse = function (callback) {
	if (this.leftChild !== undefined) {
		this.leftChild.traverse(callback);
	}

	callback(this.value);

	if (this.rightChild !== undefined) {
		this.rightChild.traverse(callback);
	}
};

BinarySearchTree.Node.prototype.remove = function (key) {
	if (this.key === key) {
		if (this.leftChild !== undefined && this.rightChild !== undefined) {
			var successor = this.rightChild.findMinimumNode();
			this.key = successor.key;
			this.value = successor.value;
			successor.remove(successor.key);
		} else if (this.leftChild !== undefined) {
			this.replace(this.leftChild);
		} else if (this.rightChild !== undefined) {
			this.replace(this.rightChild);
		} else {
			this.replace(undefined);
		}
	} else if (this.compareTo(key) > 0) {
		this.leftChild.remove(key);
	} else {
		this.rightChild.remove(key);
	}
};

BinarySearchTree.Node.prototype.get = function (key) {
	return this.find(key).value;
};

var head = new BinarySearchTree.Node(0,0);
head.insert(1,1);
head.insert(2,2);
head.insert(3,3);
head.insert(4,4);
head.insert(5,5);
