mergesort = function (array) {
	var sortedLeft, sortedRight;

	if (array.length < 2) {
		return array;
	}

	if (array.length == 2) {
		if (array[0] < array[1]) {
			return array;
		} else {
			return [array[1],array[0]];
		}
	}

	if (array.length > 2) {

		return function (sortedLeft,sortedRight) {
			if (sortedLeft.length == 0 && sortedRight.length === 0) {
				return [];
			}

			if (sortedRight.length === 0) {
				return sortedLeft;
			}

			var i = 0;
			while (sortedLeft[i] < sortedRight[0]) {
				i++;
			}
			return sortedLeft.slice(0,i).concat(arguments.callee(sortedRight,sortedLeft.slice(i)));
		}(
			mergesort(array.slice(0,array.length / 2)),
			mergesort(array.slice(array.length / 2))
		);

	}

};
