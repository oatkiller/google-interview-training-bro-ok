sort :: Ord a => [a] -> [a]
sort xs@[] = xs
sort xs@[x] = xs
sort xs = merge (sort fs) (sort ss)
	where
		(fs,ss) = split xs

		split :: [a] -> ([a],[a])
		split xs = (fs,ss)
			where
				(fs,ss) = step xs xs
					where
						step (x:xs) (_:_:ys) = (x:rs,os)
							where
								(rs,os) = step xs ys
						step xs _ = ([],xs)

		merge :: Ord a => [a] -> [a] -> [a]
		merge xs [] = xs
		merge [] ys = ys
		merge (x:xs) (y:ys)
			| x < y = x : merge xs (y:ys)
			| otherwise = y : merge (x:xs) ys
