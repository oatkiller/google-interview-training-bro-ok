var compareArrays = function (array1,array2) {
	if (array1.length !== array2.length) {
		return false;
	}
	if (array1[0] !== array2[0]) {
		return false;
	}
	if (array1.length < 2) {
		return true;
	} else {
		return compareArrays(array1.slice(1),array2.slice(1));
	}
};

assertArraysAreTheSame = function (array1,array2) {
	if (compareArrays(array1,array2) !== true) {
		console.error("array: ",array1," is not similar to ",array2);
		throw new Error();
	}
};

assertArraysAreTheSame(mergesort([]),[]);
assertArraysAreTheSame(mergesort([1]),[1]);
assertArraysAreTheSame(mergesort([1,2]),[1,2]);
assertArraysAreTheSame(mergesort([2,1]),[1,2]);
assertArraysAreTheSame(mergesort([1,2,3]),[1,2,3]);
assertArraysAreTheSame(mergesort([2,1,3]),[1,2,3]);
assertArraysAreTheSame(mergesort([1,2,3,4]),[1,2,3,4]);
assertArraysAreTheSame(mergesort([2,1,3,4]),[1,2,3,4]);
assertArraysAreTheSame(mergesort([2,1,4,3]),[1,2,3,4]);
assertArraysAreTheSame(mergesort([4,3,2,1]),[1,2,3,4]);
assertArraysAreTheSame(mergesort([4,3,2,1,0]),[0,1,2,3,4]);
assertArraysAreTheSame(mergesort([5,4,3,2,1,0]),[0,1,2,3,4,5]);
assertArraysAreTheSame(mergesort([0,5,1,4,3,2]),[0,1,2,3,4,5]);
