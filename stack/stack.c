#include <stdlib.h>
#include <stdio.h>

typedef struct Node {
	int value;
	struct Node * next;
} Node;

struct Node * create_node () {
	struct Node * node = (struct Node *) malloc(sizeof(struct Node));
	node->value = 0;
	node->next = NULL;
	return node;
}

struct Node * set_value (struct Node * node, int value) {
	node->value = value;
	return node;
}

struct Node * add_to_node (struct Node * node,int value) {
	if (node->value == 0) {
		node->value = value;
		return node;
	} else {
		if (node->next == NULL) {
			node->next = create_node();
		}
		return add_to_node(node->next,value);
	}
}

void main () {

	Node * current, * head;

	int i;

	head = create_node();

	for (i = 0; i < 1000; i++) {
		add_to_node(head,i);
	}

	current = head;

	while (current) {
		printf("%d\n", current->value);
		current = current->next;
	}
}
