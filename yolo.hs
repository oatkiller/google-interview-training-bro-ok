import Data.List

step (1:1:1:zs) = 3:1:step zs
step (1:1:ys) = 2:1:step ys
step (1:xs) = 1:1:step xs

step (2:2:2:zs) = 3:2:step zs
step (2:2:ys) = 2:2:step ys
step (2:xs) = 1:2:step xs

step (3:3:3:zs) = 3:3:step zs
step (3:3:ys) = 2:3:step ys
step (3:xs) = 1:3:step xs

step ([]) = []

look_and_say = [1]:map step look_and_say

counts xs = counts' 0 0 0 xs
	where
		counts' a b c (x:xs) = 
			case x of
				1 -> counts' (succ a) b c xs
				2 -> counts' a (succ b) c xs
				3 -> counts' a b (succ c) xs
		counts' a b c ([]) = (a,b,c)

sequence_counts n = intercalate "," $ map show [a, b, c]
	where
		(a,b,c) = counts $ look_and_say !! n

patterns = map (sequence_counts) [0..39]

four_hundred_nineteen = sequence_counts 99

main = putStrLn $ show $ four_hundred_nineteen
