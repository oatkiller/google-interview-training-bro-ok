quicksort :: Ord a => [a] -> [a]
quicksort xs@[] = xs
quicksort xs@[x] = xs
quicksort (x:xs) = (quicksort lesser) ++ equal ++ (quicksort greater)
	where
		(lesser,equal,greater) = parition xs
			where
				parition = foldl operator ([],[x],[])
					where
						operator (lesser,equal,greater) y
							| y < x 		= (y:lesser,equal,greater)
							| y == x 		= (lesser,y:equal,greater)
							| otherwise = (lesser,equal,y:greater)
